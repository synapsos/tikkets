from django.conf.urls import *
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

from tikkets.models import Ticket
from django.contrib.auth.models import User
from tikkets.views import *

from registration.views import register, activate

from django_tables2_simplefilter import FilteredSingleTableView
from tikkets.tables import TicketTable, UserTable

urlpatterns = patterns('',
	url(r'^$', 'tikkets.views.home', name='home'),
#	url(r'^$', FilteredSingleTableView.as_view(
#			template_name='home.html',
#			table_class=TicketTable,
#			model=Ticket, # .objects.all().filter(assignee=2)
#			table_pagination={"per_page": 50}
#		),
#		name='home'),
	#url(r'^t/all/$', 'tikkets.views.ticket_list', name='ticket_list'), # r'^$'
	url(r'^settings/',
		direct_to_template,
		{'template': 'tikkets/settings.html'},
		name='settings'
	),
	url(r'^t/all/$',
		FilteredSingleTableView.as_view(
			template_name='ticket_list.html',
			table_class=TicketTable,
			model=Ticket, 
			table_pagination={"per_page": 50}
		),
		name='ticket_list'
	),
	url(r'^t/new/$', TicketCreateView.as_view(), name='ticket_create'),
	url(r'^t/(?P<pk>\d+)/$', TicketDetailView.as_view(), name='ticket_detail'),
	url(r'^t/(?P<pk>\d+)/edit/$', TicketEditView.as_view(), name='ticket_edit'),
	url(r'^t/(?P<pk>\d+)/delete/$', TicketDeleteView.as_view(), name='ticket_delete'),
	
	url(r'^t/all/json$', 'tikkets.views.tickets_json', name='ticket_json'),
	url(r'^t/(?P<id>\d+)/json$', 'tikkets.views.ticket_json', name='ticket_json'),
	
	#url(r'^u/all/', 'tikkets.views.user_list', name='user_list'),
	url(r'^u/all/$',
		FilteredSingleTableView.as_view(
			template_name='user_list.html',
			table_class=UserTable,
			model=User, 
			table_pagination={"per_page": 50}
		),
		name='user_list'
	),
	url(r'^u/new/$', UserCreateView.as_view(), name='user_create'),
	url(r'^u/(?P<pk>\d+)/$', UserDetailView.as_view(), name='user_detail'),
	url(r'^u/(?P<pk>\d+)/edit/$', UserEditView.as_view(), name='user_edit'),
	
	url(r'^u/all/json$', 'tikkets.views.users_json', name='users_json'),
	url(r'^u/(?P<id>\d+)/json$', 'tikkets.views.user_json', name='user_json'),
	
	url(r'^search/$', 'tikkets.views.search', name='search'),
	
	url(r'^login/$', 'tikkets.views.login_view', name='login'), # LoginView.as_view()
	url(r'^logout/$', 'tikkets.views.logout_view', name='logout'),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	url(r'^admin/', include(admin.site.urls)),
	
	url(r'^activate/complete/$',
		direct_to_template,
		{'template': 'registration/activation_complete.html'},
		name='registration_activation_complete'),
	# Activation keys get matched by \w+ instead of the more specific
	# [a-fA-F0-9]{40} because a bad activation key should still get to the view;
	# that way it can return a sensible "invalid key" message instead of a
	# confusing 404.
	url(r'^activate/(?P<activation_key>\w+)/$',
		activate,
		{'backend': 'registration.backends.default.DefaultBackend'},
		name='registration_activate'),
	url(r'^register/$',
		register,
		{'backend': 'registration.backends.default.DefaultBackend'},
		name='registration_register'),
	url(r'^register/complete/$',
		direct_to_template,
		{'template': 'registration/registration_complete.html'},
		name='registration_complete'),
	(r'', include('registration.auth_urls')),
	
	(r'^comments/', include('django.contrib.comments.urls')),
	(r'^i18n/', include('django.conf.urls.i18n')),
	url('^markdown/', include( 'django_markdown.urls')),
)
