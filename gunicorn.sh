﻿#!/bin/bash
set -e
LOGFILE=/srv/django/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
# user/group to run as
USER= gunicorn
GROUP= gunicorn
ADDRESS=192.168.56.101:8000
cd /srv/django
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn_django -w $NUM_WORKERS --bind=$ADDRESS \
  --user=$USER --group=$GROUP --log-level=debug \
  --log-file=$LOGFILE 2>>$LOGFILE