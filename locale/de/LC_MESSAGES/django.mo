��    P      �  k         �     �     I     \     t     {     �  
   �     �     �     �     �     �  B   �     #     *     3  .   H     w     �     �     �     �     �     �     �     �     �     	     	     	  	   	     #	     )	  	   0	     :	     C	     H	     b	     	  
   �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	      
     	
      
  
   6
     A
     H
     M
     T
     b
     k
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
            
     _    �   ~          $     B  
   H     S  
   i  
   t          �     �  
   �  Q   �  	     	        &  9   ;     u  
   �  	   �     �     �     �     �     �               "  
   )     4     F     Z     a     m     r     {     �  1   �     �     �     �               #  	   ,     6     C     R     W     c     l  
   u     �     �     �  
   �     �  	   �     �  
   �     �            	        (     /     N     U     a     i     w     }     �     �     �     �     �     �     �     7   E   H   )         0       8                    $      B   ?                 +       P   2      <       >         ,   L      3   *       M   D                              	      '       /   O   C       @              .      (   K   9       N      %       A       -       6   1       #   :      &         4   I             G                     !      =   J       
   ;   "   F         5       <em>tikkets</em> is a brand-new, easy-to-use Ticket Management System. It's also completely free - So what are you waiting for? Account activated! Activate selected users Active Activity Add Comment Admin home Administrative Assigned Ticket Assigned Tickets Assigned to Assignee Automatically fills in your username when you create a new ticket. Cancel Category Change %s's password Check your email inbox for an activation link! Close selected tickets Comments Create Create a new ticket Create a new user Created Creator Deactivate selected users Delete Description E-Mail Edit Edit Ticket Edit User Email Export Full Name Hardware Home Invalid login information Invalid username or password Joined Last Login Last activity Login Login Details Logout Miscellaneous New Ticket New User No Opened Password Persons Priority Register Register a new account Registration complete Resolution Rights Save Search Searching for Settings Simply the best. Software Staff Status This account was disabled Ticket Ticket List Tickets Tikkets Admin Title User User List Username Users Website home Welcome! Yes Your Tasks Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-11 17:10+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 <em>tikkets</em> ist ein brandneues, nutzerfreundliches Ticket Management System. Es ist außerdem komplett kostenlos - worauf warten Sie also noch? Konto aktiviert! Aktiviere ausgewählte Nutzer Aktiv Aktivität Kommentar hinzufügen Startseite Verwaltung Zugewiesenes Ticket Zugewiesene Tickets Zugewiesen an Zuständig Ihr Benutzername wird beim Erstellen eines neuen Tickets automatisch eingetragen. Abbrechen Kategorie %ss Passwort ändern Ein Aktivierunglink wurde an Ihre E-Mail-Adresse gesandt. Auswahl schließen Kommentare Erstellen Ein neues Ticket anlegen Einen neuen Benutzer anlegen Erstellt am Erstellt von Deaktiviere ausgewählte Nutzer Löschen Beschreibung E-Mail Bearbeiten Ticket bearbeiten Benutzer bearbeiten E-Mail Exportieren Name Hardware Start Ungültige Anmeldeinformationen Bitte überprüfen Sie Benutzernamen und Passwort Registriert am Letzte Anmeldung Letzte Aktivität Anmelden Anmeldeinformationen Abmelden Sonstiges Neues Ticket Neuer Benutzer Nein Erstellt am Passwort Personen Priorität Registrieren Ein neues Benutzerkonto anlegen Registrierung abgeschlossen Resolution Rechte Speichern Suchen Suche nach Einstellungen Einfach besser. Software Redakteur Status Dieses Konto wurde deaktiviert Ticket Ticketlsite Tickets Tikkets-Admin Titel Benutzer Benutzerliste Benutzername Benutzer Webseite Willkommen! Ja Ihre Aufgaben 