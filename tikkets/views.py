from django import forms
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect as redirect

from tikkets.models import Ticket
from django.contrib.auth.models import User
from tikkets.forms import LoginForm
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django_tables2 import RequestConfig
from tables import TicketTable, UserTable
import tikkets.signals

from crispy_forms.helper import FormHelper
#from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit

from dojoserializer import serialize
import re
from django.db.models import Q

from django.utils.translation import ugettext as _


### dojoserializer

@login_required
def tickets_json(request):
	tickets = Ticket.objects.all()
	json_data = serialize(tickets)
	return HttpResponse(json_data, mimetype="application/json")
	
@login_required
def ticket_json(request, id):
	ticket = Ticket.objects.get(id=id)
	json_data = serialize(ticket)
	return HttpResponse(json_data, mimetype="application/json")
	
@login_required
def users_json(request):
	users = User.objects.all()
	json_data = serialize(users)
	return HttpResponse(json_data, mimetype="application/json")
	
@login_required
def user_json(request, id):
	user = User.objects.get(id=id)
	json_data = serialize(user)
	return HttpResponse(json_data, mimetype="application/json")
	
### Search

def normalize_query(query_string,
		findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
		normspace=re.compile(r'\s{2,}').sub):
	''' Splits the query string in invidual keywords, getting rid of unecessary spaces
		and grouping quoted words together.
		Example:
		
		>>> normalize_query('  some random  words "with   quotes  " and   spaces')
		['some', 'random', 'words', 'with quotes', 'and', 'spaces']
	
	'''
	return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

def get_query(query_string, search_fields):
	''' Returns a query, that is a combination of Q objects. That combination
		aims to search keywords within a model by testing the given search fields.
	
	'''
	query = None # Query to search for every search term        
	terms = normalize_query(query_string)
	for term in terms:
		or_query = None # Query to search for a given term in each field
		for field_name in search_fields:
			q = Q(**{"%s__icontains" % field_name: term})
			if or_query is None:
				or_query = q
			else:
				or_query = or_query | q
		if query is None:
			query = or_query
		else:
			query = query & or_query
	return query

@login_required
def search(request):
	query_string = ''
	found_entries = None
	if ('q' in request.GET) and request.GET['q'].strip():
		query_string = request.GET['q']
		
		ticket_query = get_query(query_string, ['title', 'description',])
		user_query = get_query(query_string, ['username', 'first_name', 'last_name', 'email',])
		#TODO: Search for comments
		
		found_tickets = Ticket.objects.filter(ticket_query).order_by('open_date')
		found_users = User.objects.filter(user_query).order_by('date_joined')
		
		ticket_table = TicketTable(
			found_tickets,
			sequence=('id', 'title', 'description', '...'),
		)
		user_table = UserTable(
			found_users,
		)
		RequestConfig(request).configure(ticket_table)
		RequestConfig(request).configure(user_table)
		
		return render_to_response(
			'tikkets/search.html',
			{ 'query_string': query_string, 'found_tickets': found_tickets, 'found_users': found_users, 'ticket_table': ticket_table, 'user_table': user_table,},
			context_instance=RequestContext(request)
		)
	else:
		return redirect('/')

### Home

def home(request):
	if (request.user.is_authenticated()):
		queryset = Ticket.objects.filter(assignee=request.user, closed=False)
		table = TicketTable(
			queryset,
			exclude='closed',
			sequence=('id', 'title', 'description', '...'),
		)
		RequestConfig(request).configure(table)
		return render(
			request,
			"tikkets/home.html", 
			{"tasks": table},
		)
	else:
		return render(request, "welcome.html")


### Login

def login_user(request):
	username = request.POST.get('username')
	password = request.POST.get('password')
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			login(request, user)
			if (request.GET.get('next')): #### DOESN'T WORK!!!
				return redirect(request.GET.get('next'))
			else:
				return redirect('/')
		else:
			# Return a 'disabled account' error message
			error = _("This account was disabled")
			return render(request, "login.html", {"form": LoginForm(), "error": error})
	else:
		# Return an 'invalid login' error message.
		error = _("Invalid login information")
		return render(request, "login.html", {"form": LoginForm(), "error": error})

def login_view(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			return login_user(request)
		else:
			error = _("Invalid username or password")
			return render(request, "login.html", {"form": LoginForm(), "error": error})
	else:
		error = ""
		return render(request, "login.html", {"form": LoginForm(), "error": error})
	
### Logout

def logout_view(request):
	logout(request)
	return redirect('/login/')
	
### User Views

class UserCreateView(CreateView):
	model = User
	template_name_suffix = '_create'
	success_url = '/u/all'
	
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(UserCreateView, self).dispatch(*args, **kwargs)

class UserDetailView(DetailView):
	
	model = User
	
	def get_context_data(self, **kwargs):
		context = super(UserDetailView, self).get_context_data(**kwargs)
		context['created_tickets'] = Ticket.objects.all().filter(creator=self.object.id)
		context['assigned_tickets'] = Ticket.objects.all().filter(assignee=self.object.id)
		return context
		
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(UserDetailView, self).dispatch(*args, **kwargs)
		
class UserEditView(UpdateView):
	
	model = User
	template_name_suffix = '_edit'
	success_url = '../'
	
	def get_context_data(self, **kwargs):
		context = super(UserEditView, self).get_context_data(**kwargs)
		return context
		
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(UserEditView, self).dispatch(*args, **kwargs)
	
### Ticket Views

class TicketCreateView(CreateView):
	model = Ticket
	template_name_suffix = '_create'
	success_url = '/t/all'
	
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TicketCreateView, self).dispatch(*args, **kwargs)
	

class TicketDetailView(DetailView):
	
	model = Ticket
	
	def get_context_data(self, **kwargs):
		context = super(TicketDetailView, self).get_context_data(**kwargs)
		context['assignees'] = User.objects.all().filter(assigned_tickets=self.object.id)
		return context
		
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TicketDetailView, self).dispatch(*args, **kwargs)
		
	
class TicketEditView(UpdateView):
	
	model = Ticket
	template_name_suffix = '_edit'
	success_url = '../'
	
	def get_context_data(self, **kwargs):
		context = super(TicketEditView, self).get_context_data(**kwargs)
		#context['assignees'] = User.objects.all().filter(assigned_tickets=self.object.id)
		return context
		
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TicketEditView, self).dispatch(*args, **kwargs)
		
		
class TicketDeleteView(DeleteView):
	
	model = Ticket
	template_name_suffix = '_delete'
	success_url = '/t/all'
	
	def get_context_data(self, **kwargs):
		context = super(TicketDeleteView, self).get_context_data(**kwargs)
		#context['assignees'] = User.objects.all().filter(assigned_tickets=self.object.id)
		return context
		
	@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(TicketDeleteView, self).dispatch(*args, **kwargs)
		
	
### Registration

class RegistrationFormUniqueEmail(forms.Form):
	username = forms.CharField(
		max_length = 30,
		label = "username",
		required = True,
	)
	email = forms.EmailField()
	password = forms.CharField(max_length=100)
	repeat_password = forms.CharField(max_length=100)
	first_name = forms.CharField(max_length=100)
	last_name = forms.CharField(max_length=100)
	
def register_user(request):
	if request.method == 'POST': # If the form has been submitted...
		form = RegistrationFormUniqueEmail(request.POST) # A form bound to the POST data
		if form.is_valid(): # All validation rules pass
			return redirect('/complete/') # Redirect after POST
	else:
		form = RegistrationFormUniqueEmail() # An unbound form

	return render(request, 'registration_form.html', {
		'form': form,
	})
	
	
#class LoginView(FormView):
#	template_name = 'login.html'
#	form_class = LoginForm
#	success_url = '/t/all/'
#
#	def form_valid(self, form):
#		# This method is called when valid form data has been POSTed.
#		# It should return an HttpResponse.
#		form.login_user(request)
#		return super(LoginView, self).form_valid(form)
