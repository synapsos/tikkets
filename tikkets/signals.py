from django.db.models.signals import pre_save, post_save, m2m_changed
from django.dispatch import receiver
from tikkets.models import Ticket
#from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.comments.models import Comment
from django.contrib.auth.models import User
from django.contrib.comments.signals import comment_will_be_posted, comment_was_posted


@receiver(pre_save, sender=Ticket)
@receiver(m2m_changed, sender=Ticket)
def update_activity(sender, instance, **kwargs):
	instance.last_activity=timezone.now()
	
#@receiver(comment_will_be_posted, sender=Comment)
#def email_notification(sender, comment, **kwargs):
#	comment.user. #email_user(subject="Test", message="Comments!")
