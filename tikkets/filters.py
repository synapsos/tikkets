import django_filters
from tikkets.models import Ticket
from django.contrib.auth.models import User

class TicketFilter(django_filters.FilterSet):
	class Meta:
		model = Ticket
		fields = ['title', 'creator', 'status', 'closed']
		
class UserFilter(django_filters.FilterSet):
	class Meta:
		model = User
		fields = ['is_active', 'is_staff']
