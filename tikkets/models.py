from django.db.models import *
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import ugettext as _



class TicketManager(Manager):
	
	def create_ticket(title, creator, assignee, description, category, priority, status, open_date):
		ticket = self.create(
			title=title,
			creator=creator,
			assignee=assignee,
			description=description,
			category=category,
			priority=priority,
			status=status,
			open_date = timezone.now(),
		)
		return ticket
		

class Ticket(Model):

	class Meta:
		verbose_name = _('Ticket')
		verbose_name_plural = _('Tickets')
	
	CATEGORY_CHOICES = (
		('h', _('Hardware')),
		('s', _('Software')),
		('a', _('Administrative')),
		('m', _('Miscellaneous')),
	)

	PRIORITY_CHOICES = (
		(1, _('P1')),
		(2, _('P2')),
		(3, _('P3')),
		(4, _('P4')),
		(5, _('P5')),
	)

	STATUS_CHOICES = (
		('u', _('UNCONFIRMED')),
		('n', _('NEW')),
		('a', _('ASSIGNED')),
		('r', _('REOPENED')),
		('p', _('IN_PROCESS')),
		('s', _('RESOLVED')),
		('v', _('VERIFIED')),
	)
	
	RESOLUTION_CHOICES = (
		('f', _('FIXED')),
		('i', _('INVALID')),
		('w', _('WONTFIX')),
		('d', _('DUPLICATE')),
		('c', _('CANTREPRODUCE')),
		('n', _('INCOMPLETE')),
	)

	creator = ForeignKey(
		User, 
		related_name='created_tickets',
		verbose_name=_('Creator'),
	)
	assignee = ManyToManyField(
		User, 
		related_name='assigned_tickets', 
		verbose_name=_('Assigned to'),
		blank=True,
	)
	
	title = CharField(max_length=100, verbose_name=_('Title'),)
	description = TextField(verbose_name=_('Description'),)
	
	category = CharField(max_length=100, choices=CATEGORY_CHOICES, verbose_name=_('Category'),)
	priority = IntegerField(choices=PRIORITY_CHOICES, verbose_name=_('Priority'), help_text=_("P1 is the highest, P5 the lowest priority."))
	
	status = CharField(max_length=20, choices=STATUS_CHOICES, default='NEW', verbose_name=_('Status'),)
	closed = BooleanField(default=False, verbose_name=_('Closed'),)
	resolution = CharField(max_length=20, blank=True, choices=RESOLUTION_CHOICES, verbose_name=_('Resolution'),)
	
	open_date = DateTimeField(auto_now_add=True, verbose_name=_('Opened'),)
	last_activity = DateTimeField(auto_now_add=True, verbose_name=_('Last activity'),)
	
	objects = TicketManager()
	
	def __unicode__(self):
		return self.title
	
	
