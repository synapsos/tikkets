from django.contrib.auth.models import User
from tikkets.models import Ticket
from django.contrib import admin
#from django.core.mail import EmailMessage
import datetime
import tikkets.signals
from django.utils.translation import ugettext as _


class TicketInline(admin.StackedInline):
	model = Ticket.assignee.through
	verbose_name = _('Assigned Ticket')
	verbose_name_plural = _('Assigned Tickets')
	extra = 0
	
#def activate(modeladmin, request, queryset):
#	queryset.update(is_active=True)
#activate.short_description = _('Activate selected users')
#	
#def deactivate(modeladmin, request, queryset):
#	queryset.update(is_active=False)
#deactivate.short_description = _('Deactivate selected users')
#	
#class UserAdmin(admin.ModelAdmin):
#	fieldsets = [
#		(_('Login Details'),		{'fields': ['username']}),
#		(_('Full Name'),			{'fields': ['first_name', 'last_name']}),
#		(_('Email'),				{'fields': ['email']}),
#		(_('Activity'),				{'fields': ['last_login', 'is_active']}),
#		(_('Rights'),				{'fields': ['is_staff', 'is_superuser']}),
#		
#		#(None,					{'fields': ['created_tickets']}),
#		#(None,					{'fields': ['assigned_tickets']}),
#	]
#	list_display = ('username', 'first_name', 'last_name', 'last_login', 'change_password', 'is_staff', 'is_active',)
#	inlines = [TicketInline]
#	search_fields = ['username', 'first_name', 'last_name', 'email']
#	list_filter = ['is_staff', 'is_active']
#	change_form_template = 'admin/tikkets/user/change_form.html'
#	actions = [activate, deactivate]
#	
#	def change_password(self, obj):
#		# Translators: %s will be replaced with the username
#		return ('<a href="/admin/auth/user/%s/password/">' + _('Change %s\'s password') + '</a>') % (obj.id, obj.username)
#	change_password.allow_tags = True
#	change_password.short_description = _("Password")

def close(modeladmin, request, queryset):
	queryset.update(status='c')
	queryset.update(close_date = datetime.datetime.now())
close.short_description = _('Close selected tickets')

class TicketAdmin(admin.ModelAdmin):

	def save_model(self, request, obj, form, change):
		if not change:
			obj.creator = User.objects.get(pk=request.user)
		obj.save()

	fieldsets = [
		(None,					{'fields': ['creator']}),
		(None,					{'fields': ['assignee']}),
		(None,					{'fields': ['title']}),
		(None,					{'fields': ['description']}),
		(None,					{'fields': ['category']}),
		(None,					{'fields': ['priority']}),
		(None,					{'fields': ['status']}),
		#(None,					{'fields': ['open_date']}),
		#(None,					{'fields': ['last_activity']}),
		
	]
	#exclude = ('creator',)
	readonly_fields = ['creator']
	list_display = ('title', 'creator', 'status', 'priority', 'category', 'open_date', 'last_activity')
	ordering = ['open_date']
	#inlines = [UserInline]
	search_fields = ['title']
	list_filter = ['status', 'priority', 'category']
	list_select_related = True
	actions = [close]
	
	#email = EmailMessage('Notification', 'Test', t=[obj.creator.email])
	#email.save()

admin.site.register(Ticket, TicketAdmin)
