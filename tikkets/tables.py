import django_tables2 as tables
from django_tables2.utils import A  # alias for Accessor
from tikkets.models import Ticket
from django.contrib.auth.models import User
from django_tables2_simplefilter import F
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _


class TicketTable(tables.Table):
	id = tables.LinkColumn('ticket_edit', args=[A('pk')], verbose_name='#')
	title = tables.LinkColumn('ticket_detail', args=[A('pk')])
	priority = tables.Column(verbose_name='P.')
	
	def render_description(self, value):
		if (len(value) > 15):
			value = value[0:15] + '...'
		return value
	
	def render_creator(self, value, record):
		return mark_safe('''<a href=/t/all/?creator=%d>%s</a>''' % (record.creator.id, value))
	
	def render_category(self, value, record):
		return mark_safe('''<a href=/t/all/?category=%s>%s</a>''' % (record.category, value))
		
	def render_priority(self, value, record):
		return mark_safe('''<a href=/t/all/?priority=%s>%s</a>''' % (record.priority, value))
		
	def render_status(self, value, record):
		return mark_safe('''<a href=/t/all/?status=%s>%s</a>''' % (record.status, value))
		
	def render_resolution(self, value, record):
		return mark_safe('''<a href=/t/all/?resolution=%s>%s</a>''' % (record.resolution, value))
	
	class Meta:
		model = Ticket
		attrs = {"class": "paleblue"}
		sequence = ('id', 'title', 'description', 'creator', 'category', 'priority', 'status', '...')
		#exclude = ('description',)
		order_by = 'open_date'
		
		
	filters = (
		F('title',_('Title'),values_list=[(ticket.title) for ticket in Ticket.objects.all()]),
		F('description',_('Description'),values_list=[(ticket.description) for ticket in Ticket.objects.all()]),
		F('creator',_('Creator'),values_list=list(set((str(user.username), str(user.id)) for user in User.objects.filter(created_tickets__isnull=False)))),
		F('assignee',_('Assignee'),values_list=list(set((str(user.username), str(user.id)) for user in User.objects.filter(assigned_tickets__isnull=False)))),
		F('category',_('Category'),values_list=[(choice[::-1]) for choice in Ticket.CATEGORY_CHOICES]),
		F('priority',_('Priority'),values_list=[(choice[1], str(choice[0])) for choice in Ticket.PRIORITY_CHOICES]),
		F('status',_('Status'),values_list=[(choice[::-1]) for choice in Ticket.STATUS_CHOICES]),
		F('closed',_('Closed'),values_list=[(_('Yes'), '1'), (_('No'), '0')]),
		F('resolution',_('Resolution'),values_list=[(choice[::-1]) for choice in Ticket.RESOLUTION_CHOICES]),
	)
	
class UserTable(tables.Table):
	id = tables.LinkColumn('user_edit', args=[A('pk')], verbose_name='#')
	username = tables.LinkColumn('user_detail', args=[A('pk')])
	class Meta:
		model = User
		attrs = {"class": "paleblue"}
		exclude = ('password', 'user_ptr', 'is_active', 'is_superuser')
		
	filters = (
		F('username',_('Username'),values_list=[(user.username) for user in User.objects.all()]),
		F('is_active',_('Active'),values_list=[(_('Yes'), '1'), (_('No'), '0')]),
		F('is_staff',_('Staff'),values_list=[(_('Yes'), '1'), (_('No'), '0')]),
	)
